# **Les nombres complexes**

![vignette](https://vignette.wikia.nocookie.net/terminales-s1-spsp/images/8/87/Nombres_complexes.png/revision/latest?cb=20180307143600&path-prefix=fr)

## [**Première partie : Calcul algébrique**](documents/Nombrescomplexes.pdf)
## [**Deuxième partie : Géométrie**](documents/Nombres_complexes_partie_2.pdf)

